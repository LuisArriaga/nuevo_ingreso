<%-- 
    Document   : datosEscolaresP_view
    Created on : 7/06/2016, 12:19:57 PM
    Author     : IthzaVG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><b>Verifique que los datos sean correctos.</b></div>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class=" panel-heading"> <b>Datos Escolares</b></div>
        <div class="panel-body">
            <form>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <b>Clave</b>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-keyboard-o" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" readonly="">
                                <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <b>Fecha de Inicio</b>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                <input type="text" class="form-control" id="clave">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <b>Fecha de termino</b>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                <input type="text" class="form-control" id="clave">
                            </div>
                        </div>
                    </div>

                </div> 
                <div class="form-group">
                    <b>Tipo de Escuela</b>
                    <select class="form-control">
                        <option value="#"> Preparatoria Estatal</option>
                    </select>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <b>Nombre de la Instituci&oacute;n</b>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></div>
                                <input type="text" class="form-control" id="institucion">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>

    <div align="right">
        <button type="button" class="btn btn-primary" id="btnEscolares">Guardar</button>
    </div><br>
</div>