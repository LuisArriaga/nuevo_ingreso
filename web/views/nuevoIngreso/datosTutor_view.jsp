<%-- 
    Document   : datosTutor_view
    Created on : 22/06/2016, 06:55:24 PM
    Author     : IthzaVG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><b>Verifique que los datos sean correctos.</b></div>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading"><b>Datos personales</b></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <b>Nombre</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <b>Telefono</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <b>Celular</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><b>Domicilio</b></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <b>Calle</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-map-signs" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <b>Número</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-hashtag" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <b>Colonia</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-map-pin" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <b>Código Postal</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-hashtag" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <b>Estado de nacimiento</b>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-map-o" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <b> Ciudad de Nacimiento</b>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-flag" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <b>Centro de trabajo</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <div align="right">
        <button type="button" class="btn btn-primary" id="btnTutor">Guardar</button>
    </div><br>
</div>
