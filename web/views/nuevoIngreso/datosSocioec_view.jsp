<%-- 
    Document   : datosSocioec_view
    Created on : 22/06/2016, 07:28:06 PM
    Author     : IthzaVG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><b>Verifique que los datos sean correctos.</b></div>
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="form-group">
                    <b>Zona de procedencia</b>
                    <select class="form-control">
                        <option value="#">Urbano</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-6">
                <div class="form-group">
                    <b>Becado</b>
                    <select class="form-control">
                        <option value="#">No</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <b>Prog. Oprtunidades</b>
                    <select class="form-control">
                        <option value="#">No</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="form-group">
                    <b>Vive en casa</b>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-home" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" readonly="">
                        <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <b>No. de Cuartos</b>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-home" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" readonly="">
                        <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <b>Personas que viven en su casa</b>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="numero" placeholder="" size="4">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="form-group">
                    <b>Total de ingresos</b>
                    <select class="form-control">
                        <option value="#">Seleccione</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="form-group">
                    <b>Dependencia económica</b>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="numero" placeholder="" size="4">
                    </div>
                </div>
            </div>
        </div>   
    </div>
     <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="form-group">
                    <b>Personas que dependen</b>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="numero" placeholder="" size="4">
                    </div>
                </div>
            </div>
        </div>   
    </div>
    <div align="right">
        <button type="button" class="btn btn-primary" id="btnSocioec">Guardar</button>
    </div><br>
</div>
