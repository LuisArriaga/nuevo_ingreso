<%-- 
    Document   : datosPersonales_view
    Created on : 9/05/2016, 02:10:09 PM
    Author     : IthzaVG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- Inicia datos personales-->
        <br>
        <div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><b>Verifique que los datos sean correctos.</b></div> 
        <form>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <!-- Inicia informacion personal -->
                    <div class="panel-heading"><b>Informaci&oacute;n Personal</b></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>Sexo</b>
                                    <select class="form-control">
                                        <option value="#"> Femenino</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <b>Estado civil </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-universal-access"></i></span>
                                    <input type="text" class="form-control" readonly="">
                                    <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b> T.Sangre</b>
                                    <select class="form-control">
                                        <option value="#"> A+</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b> C. auditiva</b>
                                    <select class="form-control">
                                        <option value="#"> No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>Tel&eacute;fono</b>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        <input type="text" class="form-control" id="numero" placeholder="" size="4">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <b> Celular</b>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></div>
                                        <input type="text" class="form-control" id="celular">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <b> Correo electr&oacute;nico</b>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></div>
                                <input type="email" class="form-control" id="email">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Inicia lugar de nacimiento-->
            <div class="col-md-5 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Lugar de nacimiento</b></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <b>Fecha de nacimiento</b>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                <input type="text" class="form-control" id="fechaNac" placeholder="dd/mm/aaaa">
                            </div>
                        </div>
                        <div class="form-group">
                            <b>Pa&iacute;s de nacimiento</b>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" readonly="">
                                <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <b> Estado de Nacimiento</b>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-o" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" readonly="">
                                <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <b>Ciudad de nacimiento</b>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-flag" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" readonly="">
                                <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Inicia Domicilio-->
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Domicilio</b></div>
                    <div class="panel-body">

                        <div class="form-group">
                            <b>Calle</b>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-map-signs" aria-hidden="true"></i></div>
                                <input type="text" class="form-control" id="calle">
                            </div>
                        </div>

                        <div class="form-group">
                            <b>Colonia</b>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-map-pin" aria-hidden="true"></i></div>
                                <input type="text" class="form-control" id="colonia">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>N&uacute;mero</b>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-hashtag" aria-hidden="true"></i></div>
                                        <input type="text" class="form-control" id="calle">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>C&oacute;digo Postal</b>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-hashtag" aria-hidden="true"></i></div>
                                        <input type="text" class="form-control" id="calle">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>Estado</b>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-o" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" readonly="">
                                        <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>Ciudad</b>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-flag" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" readonly="">
                                        <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div align="right">
                <button type="button" class="btn btn-primary" id="btnPersonales">Guardar</button>
            </div><br>
        </form>
    </body>
</html>

<!--<div align="right">
           <button type="button" class="btn btn-primary" id="btnCorrectos">Guardar</button>
       </div>-->