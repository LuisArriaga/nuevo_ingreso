<%-- 
    Document   : datosAlumno_view
    Created on : 4/05/2016, 10:56:09 AM
    Author     : IthzaVG
--%>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"> 
            Datos del alumno
            <a style="float: right" href="#" id="aAcademicos"><i class="fa fa-check-square-o fa-lg" id="iRefresh"></i></a>

        </h3>
    </div>
    <div class="panel-body">
        <div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Verifique que los datos sean correctos.</div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <button type="button" class="btn btn-default" id="btnInformacion" data-toggle="tooltip" title="Regresar a los datos del alumno">
                                    <i class="fa fa-server" aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-default" data-toggle="tooltip" title="Crear un usuario">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>

                            </div>
                            <div class="col-md-2">
                                <b>Bloque</b>
                                <select class="form-control">
                                    <option value="#">Bloque</option>
                                </select>
                            </div>
                            <div class="col-md-4" align="center">
                                <b>Carga académica</b><br>
                                <button type="button" class="btn btn-default"><i class="fa fa-book" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-default"><i class="fa fa-eraser" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-default"><i class="fa fa-print" aria-hidden="true"></i></button>

                            </div>
                            <div class="col-md-4" align="center">
                                <b>Curso Sabatino</b><br>
                                <button type="button" class="btn btn-default"><i class="fa fa-book" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-default"><i class="fa fa-eraser" aria-hidden="true"></i></button>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <form>

            <div class="col-md-2" align="center">
                <a href="#" class="thumbnail">
                    <img src=http://placehold.it/171x180 alt="...">
                </a>
            </div>

            <div class="col-md-5">
                <div class="form-group">
                    <label for="noctrl">No. Ctrl</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-list-ol" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="noctrl">
                    </div>
                </div>
                <div class="form-group">
                    <label for="curp">Curp</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-reorder" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="curp">
                    </div>
                </div>
                <div class="form-group">
                    <label for="apepat">Apellido Paterno</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-male" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="apepat">
                    </div>
                </div>
                <div class="form-group">
                    <label for="apepmat">Apellido Materno</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-female" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="apemat">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="nombre">
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <label for="carrera">Carrera</label>
                <select class="form-control">
                    <option value="#"> Carrera</option>
                </select>
                <br>

                <div class="form-group">
                    <label for="fecha">Fecha de Ingreso</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                        <input type="text" class="form-control" id="fecha" placeholder="dd/mm/aaaa">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="plan">Plan</label>
                            <input type="text" class="form-control" id="planId" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nombre plan</label>
                            <input type="text" class="form-control" id="planId" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="plan">Especialidad</label>
                            <input type="text" class="form-control" id="planId" placeholder="0">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nombre espec.</label>
                            <input type="text" class="form-control" id="planId" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sem">Semestre</label>
                            <input type="text" class="form-control" id="sem" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Estatus</label>
                            <select class="form-control">
                                <option value="#"> Inscrito</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
