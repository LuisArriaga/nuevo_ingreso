<%-- 
    Document   : datosPersonales_view
    Created on : 9/05/2016, 02:10:09 PM
    Author     : IthzaVG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="form-inline">
                    <h3 class="panel-title"> 
                        Datos del alumno
                        <a style="float: right" href="#" id="aAcademicos"><i class="fa fa-check-square-o fa-lg" id="iRefresh"></i></a>

                    </h3>
                </div>

            </div>
            <div class="panel-body">
                <ul role="tablist" class="nav nav-tabs" id="tabDatos">
                    <li role="presentation" class="active"><a href="#divTabPersonales"  data-toggle="tab" role="tab">Datos Personales</a></li>
                    <li role="presentation" ><a href="#divTabEscolares" aria-controls="divTabEscolares" data-toggle="tab" role="tab" >Datos Escolares</a></li>
                    <li role="presentation" ><a href="#divTabFamiliares" aria-controls="divTabFamiliares" data-toggle="tab" role="tab">Datos Familiares</a></li>
                    <li role="presentation" ><a href="#divTabTutor"  aria-controls="divTabTutor"  data-toggle="tab" role="tab">Datos del tutor</a></li>
                    <li role="presentation" ><a href="#divTabSocioec" aria-controls="divTabSocioec"  data-toggle="tab" role="tab">Datos Socioeconomicos</a></li>
                </ul>
                <br>
                <div class="tab-content">

                    <!-- Inicia datos personales-->
                    <div role="tabpanel" class="tab-pane active" id="divTabPersonales">
                        <%@include file="datosPersonales_view.jsp" %>
                    </div>

                    <!--Inicia datos escolares -->

                    <div role="tabpanel" class="tab-pane fade" id="divTabEscolares">
                        <%@include file="datosEscolaresP_view.jsp" %>
                    </div>

                    <!-- Inicia datos familiares -->

                    <div role="tabpanel" class="tab-pane fade" id="divTabFamiliares">
                        <%@include file="datosFamiliares_view.jsp" %>
                    </div>

                    <!-- Inicia datos del tutor -->
                    <div role="tabpanel" class="tab-pane fade" id="divTabTutor">
                        <%@include file="datosTutor_view.jsp" %>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="divTabSocioec">
                        <%@include file="datosSocioec_view.jsp" %> 
                    </div>
                </div>

            </div>
        </div>

    </body>
</html>
