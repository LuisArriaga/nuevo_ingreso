<%-- 
    Document   : nuevoIngreso_view
    Created on : 3/05/2016, 12:12:44 PM
    Author     : IthzaVG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link href="css/lib/bootstrap.min.css" rel="stylesheet">                 
        <link href="css/lib/font-awesome.min.css" rel="stylesheet">
        <link href="css/lib/jquery-ui.css?" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        
        <title>Nuevo Ingreso</title>
    </head>
    <body>
        <div class="container">
            <!--Inicia header -->
            <div class="row">
                <div class="col-md-12">
                    <%@include file="../general/header.jsp" %>
                </div>
            </div>
            <!--Termina Header -->
            <br>
            <div class="row">
                <div class="col-md-12">
                    <%@include file="../general/navbar.jsp" %>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <b>Ingresa ficha</b>
                        </div>
                        <div class="panel-body">
                            <div align="center">
                                <form class="form-inline">
                                    <div class="form-group">

                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
                                            <input type="text" class="form-control" id="ficha" placeholder="Ficha">
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary" id="btnCarga"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="cargaDatos">
                <div class="col-md-12">
                    <%@include file="datosAlumno_view.jsp" %>
                </div>
            </div>

            <div class="row" id="cargaAlumno" style="display:none">
                <div class="col-md-12">
                    <%@include file="datosEscolares_view.jsp" %>
                </div>
            </div>

        </div>

        <%@include file="../general/footer.jsp" %>
        <script type="text/javascript" src="js/lib/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/lib/bootstrap.min.js"></script>
        <script src="js/lib/bootbox.min.js"></script>
        <script type="text/javascript" src="js/nuevoIngreso_handler.js"></script>
    </body>
</html>
