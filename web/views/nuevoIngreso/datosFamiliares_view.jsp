<%-- 
    Document   : datosFamiliares_view
    Created on : 22/06/2016, 06:11:24 PM
    Author     : IthzaVG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><b>Verifique que los datos sean correctos.</b></div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class=" panel-heading"> <b>Datos del padre</b></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <b>Nombre del padre</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-male" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <b>Vive</b>
                        <select class="form-control">
                            <option value="">Sí</option>
                            <option value="">No</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <b>Ocupación del padre</b>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <b>Estudios del padre</b>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <b>Otra ocupación del padre</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
            </div>
            <br>

        </div>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><b>Datos de la madre</b></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <b>Nombre de la madre</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-female" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <b>Vive</b>
                        <select class="form-control">
                            <option value="">Sí</option>
                            <option value="">No</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <b>Ocupación de la madre</b>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <b>Estudios de la madre</b>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <b>Otra ocupación de la madre</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <b>Vive con</b>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-home" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" id="clave">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div align="right">
        <button type="button" class="btn btn-primary" id="btnFamiliares">Guardar</button>
    </div><br>
</div>