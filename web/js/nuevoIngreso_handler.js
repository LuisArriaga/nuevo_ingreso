/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $("#btnInformacion").click(function () {
        document.getElementById("cargaDatos").style.display = 'block';
        document.getElementById("cargaAlumno").style.display = 'none';
    });

    $("#aAcademicos").click(function () {
        document.getElementById("cargaDatos").style.display = 'none';
        document.getElementById("cargaAlumno").style.display = 'block';
    });

    $("#btnPersonales").click(function () {
        bootbox.alert({
            message: '<img src="img/correct.png" width="30px"/>\n\
 Se ha guardado <b>correctamente</b>'

        });
        $('.nav-tabs a[href="#divTabEscolares"]').tab('show');
    });

    $("#btnEscolares").click(function () {
        bootbox.alert({
            message: '<img src="img/error.png" width="30px"/>\n\
 Ha existido un error <b>Revise los datos</b>'
        });
        $('.nav-tabs a[href="#divTabFamiliares"]').tab("show");
    });

    $("#btnFamiliares").click(function () {
        bootbox.alert({
            message: '<img src="img/warning.png" width="30px"/>\n\
 Está seguro de guardar los datos?'
        });
        $('.nav-tabs a[href="#divTabTutor"]').tab("show");
    });

    $("#btnTutor").click(function () {
        bootbox.alert("Se guardó correctamente");
        $('.nav-tabs a[href="#divTabSocioec"]').tab("show");
    });

    $("#btnSocioec").click(function () {
        bootbox.alert("Se guardó correctamente");
    });


});